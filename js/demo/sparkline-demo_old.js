$(function () {
    $("#sparkline9").sparkline([150, 120, 130, 95, 150, 160, 125], {
        type: 'bar',
	chartRangeMin: 0,
        barWidth: 20,
        height: '150px',
        lineColor: '#17997f',
        fillColor: '#ffffff',});

    $("#sparkline9").sparkline([89, 90, 110, 100, 140, 160, 150], {
	composite: true, 
	chartRangeMin: 0,
        type: 'line',
        height: '150px',
        lineColor: '#000000',
	fillColor: false,
});

});
