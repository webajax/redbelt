<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Incident extends CI_Controller {

    private $key;

	public function __construct() {
        
        parent::__construct();
        $this->load->library('smartyciclass');
        $this->load->helper('url');
        $this->load->model('incident_model');
     
        
    }

	public function list(){

        #get data model
        $response = $this->incident_model->list();



        if($response["count"] >0 ){

            #result
            $data["list"]  = array_filter($response["query"]["list"]);
            $data["count"] = count($data["list"]);

        }else{

            #result
            $data          = null;
            $data["count"] = 0;

        }

        if(isset($data["list"])){
            #get last insert
            $this->key = end($data["list"]);
            $this->key = $this->key["id"];
        }    


	    #load smarty template display	
        $this->smartyciclass->display('template/header.tpl');     
		$this->smartyciclass->display('incident/incident.tpl',$data); 
        $this->smartyciclass->display('template/footer.tpl');   

    }



    public function add(){

        #add register txt

        $data = array(
        
            'id'          => $_POST['id'],
            'title'       => $_POST['title'],
            'description' => $_POST['description'],
            'criticality' => $_POST['criticality'],
            'type'        => $_POST['type'],
            'status'      => $_POST['status'],
            'pos'         => $this->key

        );



        #response add
        $response = $this->incident_model->add($data);

        #if true
        if($response["query"]){

            $data = array(

                "success"  => true,
                "message"  => "Cadastrado com sucesso!",
                "response" => $data,
                "idnow"    => $response["idnow"]

            );

        
        }else{

            $data = array(

                "success" => false,
                "message" => $data["exception"]

            );

        }


        echo json_encode($data);

    }


    public function edit(){

        #add register txt

        $data = array(
        
            'id'          => $_POST['id'],
            'title'       => $_POST['title'],
            'description' => $_POST['description'],
            'criticality' => $_POST['criticality'],
            'type'        => $_POST['type'],
            'status'      => $_POST['status'],
            'pos'         => $this->key

        );



        #response add
        $response = $this->incident_model->edit($data);

        #if true
        if($response["query"]){

            $data = array(

                "success"  => true,
                "message"  => "Editado com sucesso!",
                "response" => $data,
                "idnow"    => $response["idnow"]

            );

        
        }else{

            $data = array(

                "success" => false,
                "message" => $data["exception"]

            );

        }


        echo json_encode($data);

    }

    public function delete(){


        $id = $_POST['id'];


        #get last register txt
        $response = $this->incident_model->delete($id);
        $data=null;




        #if true
        if($response["query"]){

            $data = array(

                "success"  => true,
                "message"  => "Deletado com sucesso!",
            );

        
        }else{

            $data = array(

                "success" => false,
                "message" => $data["exception"]

            );

        }


        echo json_encode($data);        

    }



    public function view(){




        #get last register txt
        $response = $this->incident_model->view();


        if($response["count"] >0 ){

            #result
            $data["success"] = true;
            $data["list"]    = array_filter($response["query"]["list"]);
            $data["count"]   = count($data["list"]);

        }else{

            #result
            $data["success"] = false;
            $data            = null;
            $data["count"]   = 0;

        }

        if(isset($data["list"])){
            #get last insert
            $data["success"] = true;
            $this->key = end($data["list"]);
            $this->key = $this->key["id"];
        }    




        echo json_encode($data);        

    }


    public function lastReg(){

        #get last register txt
        $response = $this->incident_model->lastReg();

        echo json_encode($response);


    }


}