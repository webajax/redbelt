<?php
/* Smarty version 3.1.30, created on 2019-05-28 23:37:38
  from "/var/www/html/redbelt/application/views/template/header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5cedf072ddea36_34120642',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e6019d3d388f4191a53f56cbce128cb65a943041' => 
    array (
      0 => '/var/www/html/redbelt/application/views/template/header.tpl',
      1 => 1559097457,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cedf072ddea36_34120642 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>

    <link rel="shortcut icon" href="<?php echo base_url('img/verbisis-logo.ico');?>
" type="image/x-icon" />
 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!--<meta http-equiv="content-type" content="text/html; charset=utf-8">-->
    <title>Redbelt</title>


    <link href="<?php echo base_url('');?>
css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/style.css" rel="stylesheet">    
    <link href="<?php echo base_url('');?>
css/basic.css" rel="stylesheet">     <!-- Usado pela area de upload de arquivos -->
    <link href="<?php echo base_url('');?>
css/dropzone.css" rel="stylesheet">  <!-- Usado pela area de upload de arquivos -->
    <!--morris-->
    <link href="<?php echo base_url('');?>
css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">


    <link href="<?php echo base_url('');?>
css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/cropper/cropper.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url('');?>
css/select2.css" rel="stylesheet"> 

    <!--layers css-->
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('');?>
css/jqcloud/jqcloud.css" />   

    <!--jAlert css-->
    <link href="<?php echo base_url('');?>
css/jAlert.css" rel="stylesheet"> 

    <!--datepicker JS-->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <?php echo '<script'; ?>
 src="http://code.jquery.com/jquery-1.12.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"><?php echo '</script'; ?>
> 



    <!-- Mainly scripts -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/jquery-2.1.1.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/metisMenu/jquery.metisMenu.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/slimscroll/jquery.slimscroll.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/jeditable/jquery.jeditable.js"><?php echo '</script'; ?>
>
    <!-- Custom and plugin javascript -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/inspinia.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/pace/pace.min.js"><?php echo '</script'; ?>
>
    <!-- Flot -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/flot/jquery.flot.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/flot/jquery.flot.tooltip.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/flot/jquery.flot.resize.js"><?php echo '</script'; ?>
>
    <!-- JSKnob -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/jsKnob/jquery.knob.js"><?php echo '</script'; ?>
>
    <!-- ChartJS-->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/chartJs/Chart.min.js"><?php echo '</script'; ?>
>
    <!-- Peity -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/peity/jquery.peity.min.js"><?php echo '</script'; ?>
>
    <!-- Peity demo -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/demo/peity-demo.js"><?php echo '</script'; ?>
>
    <!-- Toggle script -->

    <!--cookie JS-->
    <!--<?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/js.cookie.js"><?php echo '</script'; ?>
>-->
    <?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/dataTables/jquery.dataTables.js"><?php echo '</script'; ?>
>    

    <?php echo '<script'; ?>
 src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"><?php echo '</script'; ?>
>


    <!--select 2-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"><?php echo '</script'; ?>
>

    <!--jAlert-->
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('');?>
js/jAlert.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('');?>
js/jAlert-functions.js"><?php echo '</script'; ?>
>


    <!--layers-->
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('');?>
js/jqcloud/jqcloud-1.0.4.js"><?php echo '</script'; ?>
>

    <!--graph-->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/morris/raphael-2.1.0.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/morris/morris.js"><?php echo '</script'; ?>
>
    <!--<?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/chartJs/Chart.min.js"><?php echo '</script'; ?>
>-->
     <?php echo '<script'; ?>
 type="text/javascript" src="https://www.gstatic.com/charts/loader.js"><?php echo '</script'; ?>
><!--google chart-->

    <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js"><?php echo '</script'; ?>
>
   
    <!-- Rickshaw -->
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/rickshaw/vendor/d3.v3.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url('');?>
js/plugins/rickshaw/rickshaw.min.js"><?php echo '</script'; ?>
>

</head>
<body class="top-navigation">
    <input type="hidden" id="base_url_logout" value="<?php echo base_url('user/logout');?>
" >

    <div id="wrapper">
        <div id="page-wrapper" class="blue-bg">
          <div class="row border-bottom white-bg">
            <nav class="navbar navbar-fixed-top" role="navigation" style="position: absolute;z-index: 3">
                <div class="navbar-header" >
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="<?php echo base_url('incident');?>
" class="navbar-brand" text-font-bold="true">
                            <img width="70" height="40" src="<?php echo base_url('img/logo-redbelt.gif');?>
">
                            </a>

                </div>
                <div class="navbar-collapse collapse" id="navbar">
                  <ul class="nav navbar-nav">

                    <li class=" " >
                      <a aria-expanded="false" role="button" href="<?php echo base_url('incident');?>
"> Home </a>
                    </li>


                 
                  <!--end carga menu-->         


                </div>

            </nav>
          </div>



<?php }
}
