

<!-- this page use smarty template all tags '<?php' not use more -->

<input type="hidden" name="url" id="url" value="{base_url('/index.php/Incident/lastReg') }">
<input type="hidden" name="url_add" id="url_add" value="{base_url('/index.php/Incident/add') }">
<input type="hidden" name="url_delete" id="url_delete" value="{base_url('/index.php/Incident/delete') }">
<input type="hidden" name="url_edit_view" id="url_edit_view" value="{base_url('/index.php/Incident/view') }">
<input type="hidden" name="url_edit" id="url_edit" value="{base_url('/index.php/Incident/edit') }">

</br>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">

				<div class="ibox-content">


					<div class="table-responsive" style="overflow-x: hidden">
						<!--<a href="#" id="btn-add" class="btn btn-primary "><i class="fa fa-plus label-bt-add"></i>&nbsp;Novo Arquivo</a>-->

						<button type="button" class="btn btn-primary btmodalShow"  style="position:relative;background-color:#1c84c6;border:none"  data-toggle="modal" data-target="#modalShowModal" title="Adicionar Registro"><i class="fa fa-plus label-bt-add" ></i>  Adicionar Incidente</button>



						<div class="pull-right">
                            <h5 class="no-margins text-right "  >{{$count}} registros(s) encontrado(s)</h5>

                        </div>



						<table class="table table-striped table-bordered table-hover dataTables-example" >
							<thead>
							<tr>

								<th>#Id</th>
								<th>Título</th>
								<th>Descrição</th>
								<th>Criticidade</th>
								<th>Tipo</th>
								<th>Status</th>
								<th>Ações</th>

							</tr>
							</thead>
							<tbody id="treg">
								{if isset($list) }
									 {foreach key=key from = $list item = $row}
											 <tr>	
												{foreach from=$row key=$key2 item=value} 
												 <td>{$value}</td>
											    {/foreach}

											    {if isset($row['id'])}
												     <td style="text-align: center">
														<a href="#" class="md-trigger "   ><button type="button" class="btn btn-success btn-xs bt-edit"  data-toggle="modal" data-target="#editShowModal" data-value="{$row['id']}" data-placement="right" title="Editar"><i class="fa fa-pencil"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-xs bt-del"  data-toggle="modal" data-target="#modalDelete" data-value="{$row['id']}" data-placement="right" title="Excluir"><i class="fa fa-times"></i></button>
												    </td>
												{/if}    
											 </tr> 
										{/foreach}
								{/if}		
							</tbody>

						</table>
					</div>
					<!--<label name="label-second-plan" id="label-second-plan">0</label>-->
				</div>
			</div>
		</div>
	</div>


</div>

<!-- Modal Incident ADD created André 17/02/2021-->
<div class="modal fade"  id="modalShowModal"   aria-labelledby="Inserir"  >
  <div class="modal-dialog"  >
		        <div class="modal-content"  >
		            <div class="modal-header div-close-process"  >
		                <button type="button" class="md-close close bt-close-modal" data-dismiss="modal" aria-hidden="true">×</button>
		                <h4 class="modal-title">Adicionar Incidente</h4>
		            </div>


		            <form name="formadd" id="formadd"  >
						<input type="hidden" name="id" id="id">

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Título</label><br>
						            <input type="type"  id="titulo" name="titulo"> 
						        </div>			       

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Descrição</label><br>
						            <input type="type"  id="descricao" name="descricao"> 
						        </div>	

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Criticidade</label><br>
						            <select class=" " name="select-criticidade" id="select-criticidade"   >
						            	 <option selected  value="Alta">Alta</option>
						            	 <option   value="Média">Média</option>
						            	 <option   value="Baixa">Baixa</option> 
						            </select>
						        </div>	


				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Tipo</label><br>
						            <select class=" " name="select-tipo" id="select-tipo" >
						            	 <option selected  value="Ataque Brute Force">Ataque Brute Force</option>
						            	 <option   value="Credenciais Vazadas">Credencias Vazadas</option>
						            	 <option   value="Ataque de DDoS">Ataque de DDoS</option>
						            	 <option   value="Atividades Anormais de Usuários">Atividades Anormais de Usuários</option> 

						            </select>
						        </div>	

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Status</label><br>
						            <select class=" " name="select-status" id="select-status" >
						            	 <option selected  value="Aberto">Aberto</option>
						            	 <option  value="Fechado">Fechado</option>
						            	 
						            </select>
						        </div>

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id"></label><br>

						        </div>

						      
			            <div class="modal-footer">
			                <button type="button" id="bt-yes" class="btn btn-sm btn-success bt-addReg " >Gravar</button>
			                <button type="button" class="btn btn-sm btn-success bt-no md-close" data-dismiss="modal">Fechar</button>
			            </div>

			    	</form>
		        </div>
		    </div>
	</div>



<!-- Modal Incident EDIT created André 17/02/2021-->
<div class="modal fade"  id="editShowModal"   aria-labelledby="Editar"  >
  <div class="modal-dialog"  >
		        <div class="modal-content"  >
		            <div class="modal-header div-close-process"  >
		                <button type="button" class="md-close close bt-close-modal" data-dismiss="modal" aria-hidden="true">×</button>
		                <h4 class="modal-title">Editar Incidente</h4>
		            </div>


		            <form name="formadd" id="formadd"  >
						<input type="hidden" name="edit-id" id="edit-id">

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Título</label><br>
						            <input type="type"  id="edit-titulo" name="edit-titulo"> 
						        </div>			       

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Descrição</label><br>
						            <input type="type"  id="edit-descricao" name="edit-descricao"> 
						        </div>	

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Criticidade</label><br>
						            <select class=" " name="edit-select-criticidade" id="edit-select-criticidade"   >
						            	 <option selected  value="Alta">Alta</option>
						            	 <option   value="Média">Média</option>
						            	 <option   value="Baixa">Baixa</option> 
						            </select>
						        </div>	


				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Tipo</label><br>
						            <select class=" " name="edit-select-tipo" id="edit-select-tipo" >
						            	 <option selected  value="Ataque Brute Force">Ataque Brute Force</option>
						            	 <option   value="Credenciais Vazadas">Credencias Vazadas</option>
						            	 <option   value="Ataque de DDoS">Ataque de DDoS</option>
						            	 <option   value="Atividades Anormais de Usuários">Atividades Anormais de Usuários</option> 

						            </select>
						        </div>	

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id">Status</label><br>
						            <select class=" " name="edit-select-status" id="edit-select-status" >
						            	 <option selected  value="Aberto">Aberto</option>
						            	 <option  value="Fechado">Fechado</option>
						            	 
						            </select>
						        </div>

				     			<div class="form-group col-md-6 col-sm-6" id="div_type">
						            <label for="id"></label><br>

						        </div>

						      
			            <div class="modal-footer">
			                <button type="button" id="bt-yes" class="btn btn-sm btn-success bt-editReg " >Salvar</button>
			                <button type="button" class="btn btn-sm btn-success bt-no md-close" data-dismiss="modal">Fechar</button>
			            </div>

			    	</form>
		        </div>
		    </div>
	</div>


	<script>
    {literal}
		 $(".btmodalShow").on('click',function() {


				 	//---------------------------initialize AJAX GET last register
				 $.ajax({
		                    cache:false,
		                    type: 'GET',
		                    url: $("#url").val() ,
		                    dataType: "json",
		                    success: function(data) {

		                    	//insert last key reg bd
		                    	$("#id").val(data);

		                    }


		         });


		 })


		 $(".bt-edit").on("click", function(){

		 	var id = $(this).data("value");


		 	//---------------------------initialize AJAX GET last register
			 $.ajax({
	                    cache:false,
	                    type: 'POST',
	                    url: $("#url_edit_view").val() ,
	                    dataType: "json",
	                    success: function(data) {

	                    	console.log(data);
	                      if(data.success){	


	                            $.each(data.list, function (i, item) {
                        


	                               if(item.id == id){

	                               		console.log(item.status)

	                               		$("#edit-id").val(item.id);
	                        			$("#edit-titulo").val(item.titulo);
	                        			$("#edit-descricao").val(item.descricao);
	                        			$("#edit-select-criticidade").val(item.criticidade).trigger('change');
	                        			$("#edit-select-tipo").val(item.tipo).trigger('change');
				                        $('#edit-select-status').append($('<option>', { 
				                            value: item.status,
				                            text : item.status 
				                        }));	                        			
	                        			$("#edit-select-status").val(item.status).trigger('change');
			                       }
			                    });	
						

						  }

	                    }


	         });


		 })



		 $(".bt-editReg").on("click",function(){

		 		//get values
		 		var id          = $("#edit-id").val();
		 		var title       = $("#edit-titulo").val();
		 		var description = $("#edit-descricao").val();
		 		var criticality = $("#edit-select-criticidade").val();
		 		var type        = $("#edit-select-tipo").val();
		 		var status      = $("#edit-select-status").val();  



		  		if(title==""){
		  			jAlert('titulo');
		  			return;
		  		}

		  		if(description==""){
		  			jAlert('descricao');
		  			return;
		  		}

		  		if(criticality==""){
		  			jAlert('criticidade');
		  			return;
		  		}

		  		if(title==""){
		  			jAlert('título');
		  			return;
		  		}

		  		if(status==""){
		  			jAlert('status');
		  			return;
		  		}		  		
		  								 				 		
		  		var rows="";

		  		  //if ok insert
				 //---------------------------initialize AJAX GET SAVE
				 $.ajax({
		                    cache:false,
		                    type: 'POST',
		                    url: $("#url_edit").val() ,
		                    data: {id:id,title:title,description:description,criticality:criticality,type:type,status:status},
		                    dataType: "json",
		                    success: function(data) {


		                    	if(data.success){


								

									var btn  = $(this),
								    show = btn.data('show'),
								    hide = btn.data('hide');


											$.jAlert({

												'title':'Sucesso',
												'content':'Editado com sucesso recarregando aguarde ...',
										        'theme': 'green',
										        'showAnimation' : show,
										        'hideAnimation' : hide,
										        'btns': { 'text': 'Fechar' },
									                'onOpen': function(alert){
																		                    
								                    window.setTimeout(function(){

									                    	alert.closeAlert();
									                    	alt=1;
									                    	window.location.reload();	
									                    	}, 1000);
									                }
									     	 });



		                    	}


		                    }


		         });



		 })		



		 $(".bt-addReg").on("click",function(){

		 		//get values
		 		var id          = $("#id").val();
		 		var title       = $("#titulo").val();
		 		var description = $("#descricao").val();
		 		var criticality = $("#select-criticidade").val();
		 		var type        = $("#select-tipo").val();
		 		var status      = $("#select-status").val();  

		 		//validation
		  		if(id==""){
		  			id=-1;
		  		}


		  		if(title==""){
		  			jAlert('titulo');
		  			return;
		  		}

		  		if(description==""){
		  			jAlert('descricao');
		  			return;
		  		}

		  		if(criticality==""){
		  			jAlert('criticidade');
		  			return;
		  		}

		  		if(title==""){
		  			jAlert('título');
		  			return;
		  		}

		  		if(status==""){
		  			jAlert('status');
		  			return;
		  		}		  		
		  								 				 		
		  		var rows="";

		  		  //if ok insert
				 //---------------------------initialize AJAX GET SAVE
				 $.ajax({
		                    cache:false,
		                    type: 'POST',
		                    url: $("#url_add").val() ,
		                    data: {id:id,title:title,description:description,criticality:criticality,type:type,status:status},
		                    dataType: "json",
		                    success: function(data) {


		                    	if(data.success){


									rows = rows + "<tr >";
								   	rows = rows + "<td  data-id='"+data.idnow+"'>"+data.idnow +"</td>";
									rows = rows + "<td  data-error='"+data.response.titulo +"'>"+data.response.titulo+"</td>";
									rows = rows + "<td  data-error='"+data.response.descricao +"'>"+data.response.descricao+"</td>";
									rows = rows + "<td  data-error='"+data.response.criticidade +"'>"+data.response.criticidade+"</td>";
									rows = rows + "<td  data-error='"+data.response.tipo +"'>"+data.response.tipo+"</td>";
									rows = rows + "<td  data-error='"+data.response.status +"'>"+data.response.status+"</td>";
									rows = rows + "<td style='text-align: center'><a href='#' class='md-trigger'   ><button type='button' class='btn btn-danger btn-xs bt-del-carga'  data-toggle='modal' data-target='#modalDelete' data-placement='right' title='Excluir'><i class='fa fa-times'></i></button>" + 
									    "<button type='button' class='btn btn-sucess btn-xs bt-edit'  data-toggle='modal' data-target='#editShowModal' data-placement='right' title='Editar'><i class='fa fa-times'></i></button></td>"
									rows = rows + "</tr>";		
									
									$("#treg").append(rows)									

									var btn  = $(this),
								    show = btn.data('show'),
								    hide = btn.data('hide');


											$.jAlert({

												'title':'Sucesso',
												'content':'Cadastrado com sucesso recarregando aguarde ...',
										        'theme': 'green',
										        'showAnimation' : show,
										        'hideAnimation' : hide,
										        'btns': { 'text': 'Fechar' },
									                'onOpen': function(alert){
																		                    
								                    window.setTimeout(function(){

									                    	alert.closeAlert();
									                    	alt=1;
									                    	location.reload();	
									                    	}, 1000);
									                }
									     	 });



		                    	}


		                    }


		         });



		 })		


		 $(".bt-del").on('click' ,function(){


		 	var id = $(this).data("value");


		 	//---------------------------initialize AJAX GET last register
			 $.ajax({
	                    cache:false,
	                    type: 'POST',
	                    url: $("#url_delete").val() ,
	                    data: {id:id},
	                    dataType: "json",
	                    success: function(data) {



	                      if(data.success){	


							var btn  = $(this),
						    show = btn.data('show'),
						    hide = btn.data('hide');
						    
							$.jAlert({

								'title':'Sucesso',
								'content':data.message,
						        'theme': 'green',
						        'showAnimation' : show,
						        'hideAnimation' : hide,
						        'btns': { 'text': 'Fechar' },
					                'onOpen': function(alert){
														                    
				                    window.setTimeout(function(){

					                    	alert.closeAlert();
					                    	alt=1;
					                    	window.location.reload();	
					                  }, 1000);
					                }
					     	 });

						  }

	                    }


	         });

		 })


		 function jAlert(field){


			var btn  = $(this),
		    show = btn.data('show'),
		    hide = btn.data('hide');


					$.jAlert({

						'title':'Oppssss!',
						'content':'O campo '+ field + ' está vazio',
				        'theme': 'red',
				        'showAnimation' : show,
				        'hideAnimation' : hide,
				        'btns': { 'text': 'Fechar' },
			                'onOpen': function(alert){
			                    window.setTimeout(function(){
			                    	alert.closeAlert();
			                    	alt=1;
			                    	}, 3000);
			                }
			     	 });

			return;


		 }


	{/literal}	 



	</script>