<?php


#Created André 17/02/2021


class Incident_model extends CI_Model {
   
    function __construct() {
		#$this->load->database();
    }
   

    public function add($data){

      #add BD

      try{        

      #file bd  
      $path = "./bd/incident.txt";  

      #------------------------------------------add
      $id          = $data["id"];
      $title       = $data["title"];
      $description = $data["description"];
      $criticality = $data["criticality"];
      $type        = $data["type"];
      $status      = $data["status"];
      $pos         = $data["pos"];

      
      $f=fopen($path,"a+",0);
      
      $respLines = $this->countLinesTXT();


      #broken line
      $quebra = chr(13).chr(10);



      if($id=="")
        $id=$pos;      

      $line = $id+1 .";".$title.";".$description.";". $criticality.";".$type .";". $status .";\n" ;



      $resp = fwrite($f,$quebra.$line,strlen($line));
      fclose($f);

      $respLinesafter =  $this->countLinesTXT();


      if($respLines < $respLinesafter){

        #----------------------------------------SUCCESS!
        $query = array(
            'query'    => true,
            'exception'=> null,
            'idnow'    => $id
        );

      }else{

        #----------------------------------------FALSE ADD REG
        $query = array(
            'query'    => false,
            'exception'=> "Registro não adicionado!"
        );
      }
     


        return $query;
    
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

            return $errorInfo;

      }


    }


    public function edit($data){

      #add BD

      try{        

      #file bd  
      $path = "./bd/incident.txt";  

      #------------------------------------------add
      $id          = $data["id"];
      $title       = $data["title"];
      $description = $data["description"];
      $criticality = $data["criticality"];
      $type        = $data["type"];
      $status      = $data["status"];
      $pos         = $data["pos"];

      

      $this->delete($id);

      $f=fopen($path,"a+",0);
      
      $respLines = $this->countLinesTXT();


      #broken line
      $quebra = chr(13).chr(10);



      if($id=="")
        $id=$pos;      

      $line = $id .";".$title.";".$description.";". $criticality.";".$type .";". $status .";\n" ;



      $resp = fwrite($f,$quebra.$line,strlen($line));
      fclose($f);

      $respLinesafter =  $this->countLinesTXT();


      if($respLines < $respLinesafter){

        #----------------------------------------SUCCESS!
        $query = array(
            'query'    => true,
            'exception'=> null,
            'idnow'    => $id
        );

      }else{

        #----------------------------------------FALSE ADD REG
        $query = array(
            'query'    => false,
            'exception'=> "Registro não adicionado!"
        );
      }
     


        return $query;
    
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

            return $errorInfo;

      }


    }


    public function delete($id){

      try{


        #file bd  
        $path = "./bd/incident.txt";  

        #------------------------------------------Open file txt
        $pointer = fopen ($path,"r");
        $text = "";

        $respLines =  $this->countLinesTXT();

         
          #--------------------------------------------------read file txt
          while (!feof ($pointer)) {

              $line = fgets($pointer,4096);#ready line

              $col = explode(";", $line , 6);#col 6 merge cols after
 


              if(isset($col[1]) ) {

                 if($col[0]!=$id and $col[0]!="" )
                      $text = $text . $col[0].";".$col[1].";".$col[2].";".$col[3].";".$col[4].";".$col[5];
                 
              
              }

          } 



           unlink($path);
           $pointer = fopen($path, "w");
           chmod($path, 0777);

          #quebra line
          $quebra = chr(13).chr(10);

          $resp = fwrite($pointer,$text,strlen($text));


        
          fclose($pointer);

          $respLinesafter =  $this->countLinesTXT();



           if($respLines != $respLinesafter){


              #----------------------------------------SUCCESS!
              $query = array(
                  'query'    => true,
                  'exception'=> null,
              );

           }else{

              #----------------------------------------FALSE ADD REG
              $query = array(
                  'query'    => false,
                  'exception'=> "Opsss!Registro não foi excluído!"
              );
           }  

          return $query; 

      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

            return $errorInfo;        

      }


    }

    public function list(){

      try{        

      #file bd  
      $path = "./bd/incident.txt";  

      #------------------------------------------Open file txt
      $pointer = fopen ($path,"r");
      $data = array();
      $query = null;

        #--------------------------------------------------read file txt
        while (!feof ($pointer)) {

            $line = fgets($pointer,4096);#ready line

            $col = explode(";", $line , 6);#col 6 merge cols after



            if(isset($col[1])){



                if($col[0]!=null or $col[0]!=""){

                  $data["list"][] = array(

                      "id"         => $col[0],
                      "titulo"     => $col[1],
                      "descricao"  => $col[2],
                      "criticidade"=> $col[3],
                      "tipo"       => $col[4],
                      "status"     => $col[5] 
                  );


                  #----------------------------------------add  result list array and total lines
                  $query = array(
                      'query'    => $data,
                      'count'    => count($data["list"]),
                      'exception'=> null
                  );              
                }  

           }
          
        }  


        return $query;

    
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

            return $errorInfo;

      }

    }



  public function view(){

      try{        

      #file bd  
      $path = "./bd/incident.txt";  

      #------------------------------------------Open file txt
      $pointer = fopen ($path,"r");
      $data = array();
      $query = null;

        #--------------------------------------------------read file txt
        while (!feof ($pointer)) {

            $line = fgets($pointer,4096);#ready line

            $col = explode(";", $line , 6);#col 6 merge cols after



            if(isset($col[1])){



                if($col[0]!=null or $col[0]!=""){

                  $data["list"][] = array(

                      "id"         => $col[0],
                      "titulo"     => $col[1],
                      "descricao"  => $col[2],
                      "criticidade"=> $col[3],
                      "tipo"       => $col[4],
                      "status"     => $col[5] 
                  );


                  #----------------------------------------add  result list array and total lines
                  $query = array(
                      'query'    => $data,
                      'count'    => count($data["list"]),
                      'exception'=> null
                  );              
                }  

           }
          
        }  


        return $query;

    
      }catch(Exception $e){

        $errorInfo = array(
            'query'    => 0,
            'exception'=>$e->getMessage
        );

            return $errorInfo;

      }

    }




    public function lastReg(){


       try{        

            #file bd  
            $path = "./bd/incident.txt";  
            
            #get last line
            $line=exec('tail -n 1 ' .$path);

            $col = explode(";", $line , 6);#col 6 merge cols after

            #get last key            
            $query = $col[0];

            return $query;
          
            }catch(Exception $e){

              $errorInfo = array(
                  'query'    => 0,
                  'exception'=>$e->getMessage
              );

                  return $errorInfo;

            }

    }


    public function countLinesTXT(){

       #------------------------------------------GET path file
        $path = "./bd/incident.txt";

        #------------------------------------------COUNT total lines txt

            #Eu utilizo o PHP_INT_MAX para apontar para a última linha do arquivo, pois SplFileObject implementa SeekableIterator.
            #Daí, como a contagem das linhas começa por 0, eu tenho que somar +1 para trazer o valor certo.
            $file = new \SplFileObject($path, 'r');
            $file->seek(PHP_INT_MAX);
            $totallines = $file->key();
        #------------------------------------------End COUNT

            return $totallines;

    }

}